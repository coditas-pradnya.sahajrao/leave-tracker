import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { ILeave, Ileaves } from 'src/app/Interface/app.type';
import { LeaveServiceService } from 'src/app/Services/leave-service.service';

@Component({
  selector: 'app-planned-leaves',
  templateUrl: './planned-leaves.component.html',
  styleUrls: ['./planned-leaves.component.scss'],
})
export class PlannedLeavesComponent implements OnInit {
  @Input() leaves: ILeave[] = [];
  @Output() changeType = new EventEmitter();

  constructor(private leavesService: LeaveServiceService) {}

  async ngOnInit() {
    this.leaves = await this.leavesService.getPlannedLeavesData();
  }

  onClick(index: number, type: string) {
    console.log(type, index);

    this.leaves[index].type = type;
  }
}
