import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlannedLeavesComponent } from './planned-leaves.component';

describe('PlannedLeavesComponent', () => {
  let component: PlannedLeavesComponent;
  let fixture: ComponentFixture<PlannedLeavesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlannedLeavesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlannedLeavesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
