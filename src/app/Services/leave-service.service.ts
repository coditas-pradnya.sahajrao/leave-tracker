import { Injectable } from '@angular/core';
import { backendLeaves } from 'src/mock-dat';

@Injectable({
  providedIn: 'root'
})
export class LeaveServiceService {

  constructor() { }
  async getPlannedLeavesData() {
    // mock call to the backend
    const leaveList = await backendLeaves();
  

    return leaveList;
  }

}
