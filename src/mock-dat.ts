import { ILeave, Ileaves } from "./app/Interface/app.type";

export const backendLeaves = async (): Promise<ILeave[]> => {
    // return [
    //     {planned:'23-06-2022', unplanned:'24-03-2022', floater:'11-03-2022'},
    //     {planned:'24-06-2022', unplanned:'25-03-2022', floater:'12-03-2022'},
    //     {planned:'25-06-2022', unplanned:'26-03-2022', floater:'13-03-2022'},
    //     {planned:'26-06-2022', unplanned:'27-03-2022', floater:'14-03-2022'}
    //  ];
    return [
        {date: '23-06-2022', type: 'planned' },
        {date: '24-06-2022', type: 'unplanned' },
        {date: '25-06-2022', type: 'floater' },
        {date: '26-06-2022', type: 'planned' },
        {date: '27-06-2022', type: 'unplanned' },
        {date: '28-06-2022', type: 'floater' },
     ];
}
